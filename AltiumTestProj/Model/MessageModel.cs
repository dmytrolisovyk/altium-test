﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltiumTestProj.Model
{
    public class MessageModel
    {
        public string User { get; set; }
        public string Message { get; set; }
    }
}
