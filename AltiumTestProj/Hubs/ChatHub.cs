﻿using AltiumTestProj.Services;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltiumTestProj.Hubs
{
    public class ChatHub : Hub
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
        private readonly IMessageHistoryService messageHistoryService;

        public ChatHub(IMessageHistoryService messageHistoryService)
        {
            this.messageHistoryService = messageHistoryService;
        }

        public async Task SendMessage(string user, string message)
        {
            messageHistoryService.AddToHistory(user, message);
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task SendUsersCount(string count)
        {
            await Clients.All.SendAsync("UsersCount", count);
        }

        public async override Task OnConnectedAsync()
        {
            ConnectedIds.Add(Context.ConnectionId);
            await SendUsersCount(ConnectedIds.Count + "");
            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            ConnectedIds.Remove(Context.ConnectionId);
            await SendUsersCount(ConnectedIds.Count + "");
            await base.OnDisconnectedAsync(exception);
        }
    }
}
