﻿"use strict";

var connection = null;///new signalR.HubConnectionBuilder().withUrl("/chatHub").build();
var userName = "";

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;



function addMessage(user, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = user + ": " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
};

function initConnection() {
    connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();
    connection.on("ReceiveMessage", addMessage);
    connection.on("UsersCount", function (count) {
        document.getElementById("usersCount").innerHTML = "Users count: " + count;
    });

    connection.start().then(function () {
        document.getElementById("sendButton").disabled = false;
    }).catch(function (err) {
        return console.error(err.toString());
    });
}



document.getElementById("sendButton").addEventListener("click", function (event) {
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", userName, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

document.getElementById("loginButton").addEventListener("click", function (event) {

    userName = document.getElementById("userName").value;
    fetch("/api/login", { method: 'GET' })
        .then(res => {
            console.log("login ok");
            document.getElementById("message").classList.remove('hidden');
            document.getElementById("login").classList.add('hidden');

            fetch("/api/MessageHistory")
                .then(res => {
                    console.log("Result:", res);
                    res.json()
                        .then(msgs => {
                            msgs.map(msg => addMessage(msg.user, msg.message));
                            initConnection();
                        })                    
                });
        })
        .catch(er => console.log("error"))


    event.preventDefault();
});