﻿using AltiumTestProj.Services;
using Microsoft.AspNetCore.Mvc;

namespace AltiumTestProj.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageHistoryController : Controller
    {
        private readonly IMessageHistoryService messageHistoryService;

        public MessageHistoryController(IMessageHistoryService messageHistoryService)
        {
            this.messageHistoryService = messageHistoryService;
        }

        public IActionResult Index()
        {
            return new OkObjectResult(messageHistoryService.MessageHistory);
        }
    }
}
