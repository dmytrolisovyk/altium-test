﻿using Microsoft.AspNetCore.Mvc;

namespace AltiumTestProj.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        [HttpPost]
        public IActionResult Login()
        {
            return Ok();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }
    }
}
