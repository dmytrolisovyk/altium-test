﻿using AltiumTestProj.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltiumTestProj.Services
{
    public class MessageHistoryService : IMessageHistoryService
    {
        private object syncContext = new object();
        private List<MessageModel> messageHistory = new List<MessageModel>();

        /// <summary>
        /// Must be stored in database
        /// </summary>
        public IReadOnlyList<MessageModel> MessageHistory => messageHistory;

        public void AddToHistory(string user, string message)
        {
            lock (syncContext)
            {
                messageHistory.Add(new MessageModel { User = user, Message = message });
            }
        }
    }       
}
