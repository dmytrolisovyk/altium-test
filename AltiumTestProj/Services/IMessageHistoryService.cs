﻿using AltiumTestProj.Model;
using System.Collections.Generic;

namespace AltiumTestProj.Services
{
    public interface IMessageHistoryService
    {
        IReadOnlyList<MessageModel> MessageHistory { get; }

        void AddToHistory(string user, string message);
    }
}